<?php

header("Content-Type: text/plain");

$config = json_decode(file_get_contents("/config/config.json"), true);

$template = "#!ipxe
kernel http://{$config['host']}/winpe/wimboot
initrd http://{$config['host']}/winpe/amd64/media/Boot/BCD BCD
initrd http://{$config['host']}/winpe/amd64/media/Boot/boot.sdi boot.sdi
initrd http://{$config['host']}/winpe/amd64/media/sources/boot.wim boot.wim
boot";

print($template);