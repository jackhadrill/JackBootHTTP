<?php

header("Content-Type: text/plain");

$config = json_decode(file_get_contents("/config/config.json"), true);

$template = "#!ipxe
:start
menu Select target boot device
item --gap --            iSCSI targets
item --key i iscsiboot   Boot from JackNet AD assigned iSCSI disks
item --key i iscsihook   Hook a JackNet AD assigned iSCSI disk

item --gap --            OS installers
item --key w winpe       Boot WinPE
item --key n netbootxyz  Boot netboot.xyz

item --gap --            Advanced options
item --key c config      Configure settings
item shell               Drop to iPXE shell
item reboot              Reboot computer
item
item --key x exit        Exit iPXE and continue BIOS boot

choose selected
goto \${selected} || goto shell

# Boot from an iSCSI target.
:iscsiboot
login
params
param username \${username:uristring}
param password \${password:uristring}
chain http://{$config['host']}/ipxe/iscsi-boot.php##params
goto start

# Hook an iSCSI target and then continue with normal boot process.
:iscsihook
login
params
param username \${username:uristring}
param password \${password:uristring}
chain http://{$config['host']}/ipxe/iscsi-hook.php##params
goto start

# Boot from WinPE.
:winpe
chain http://{$config['host']}/ipxe/winpe.php
goto start

# Boot Netboot.xyz
:netbootxyz
chain http://boot.netboot.xyz/menu.ipxe
goto start

# Utility functions.

:shell
echo Type 'exit' to return to the menu
shell
goto start

:reboot
reboot

:exit
exit";

print($template);