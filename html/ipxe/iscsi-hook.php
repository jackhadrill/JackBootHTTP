<?php

header("Content-Type: text/plain");

$config = json_decode(file_get_contents("/config/config.json"), true);

$menuOptions = "";
$gotoSections = "";
foreach($config["iscsi-targets"] as $target) {
    $id = uniqid();
    $menuOptions .= "item {$id} {$target['name']}\r\n";
    $gotoSections .= ":{$id}\r\n";
    $gotoSections .= "sanhook --drive 0x80 {$target['target']}\r\n";
    $gotoSections .= "goto back\r\n";
};

$template = "#!ipxe
menu Select an iSCSI target to hook
item --gap -- iSCSI targets
{$menuOptions}
item
item back Back to main menu 
choose selected
goto \${selected} || goto shell

{$gotoSections}

:back
chain http://{$config['host']}/ipxe/main-menu.php";

print($template);